﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camaraScript : MonoBehaviour
{

    public GameObject bola;
    private Vector3  posicionRelativa;
    // Start is called before the first frame update
    void Start()
    {
        posicionRelativa = transform.position - bola.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = posicionRelativa + bola.transform.position;
    }
}
